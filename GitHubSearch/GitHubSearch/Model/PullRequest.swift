//
//  PullRequest.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

struct PullRequest: Codable {
    
    var title: String = ""
    var body: String?
    var user: User?
    var url: String = ""
    var date: String = ""
    
    enum CodingKeys: String, CodingKey {
        case title
        case body
        case user
        case url = "html_url"
        case date = "created_at"
    }
    
}
