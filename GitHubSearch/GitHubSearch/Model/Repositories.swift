//
//  Repositories.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

struct Repositories: Codable {
    
    var totalCount: Int = 0
    var incompleteResults: Bool = false
    var repository: [Repository] = []

    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case repository = "items"
    }
    
}

struct Repository: Codable {
    var name: String = "Sem nome"
    var description: String = "Sem descrição"
    var user: User?
    var forks: Int = 0
    var stars: Int = 0
    
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case user = "owner"
        case forks
        case stars = "stargazers_count"
    }
    
}

struct User: Codable {
    var login: String = "Sem login"
    var avatarUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
    }
}
