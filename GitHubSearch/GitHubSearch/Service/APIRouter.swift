//
//  APIRouter.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 01/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import Foundation
import Alamofire

class APIRouter {
    
    //MARK: - Get Repositories
    static func getRepositories(by language: String, page: Int) -> URLRequestConvertible {
        
        var urlComponents = URLComponents(string: Defines.URL_REPOSITORIES)!
        
        urlComponents.queryItems = [
            URLQueryItem(name: "q", value: "language:\(language)"),
            URLQueryItem(name: "sort", value: "stars"),
            URLQueryItem(name: "page", value: "\(page)")
        ]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 20
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request as URLRequestConvertible
    }
    
    //MARK: - Get PullRequests
    static func getPullRequests(by creator: String, repository: String) -> URLRequestConvertible {
        
        let url = URL(string: "\(Defines.URL_PULLREQUESTS)/\(creator)/\(repository)/pulls")
        
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        request.timeoutInterval = 20
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request as URLRequestConvertible
    }
    
}

