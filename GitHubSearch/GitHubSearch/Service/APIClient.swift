//
//  APIClient.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 01/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    
    //MARK: - Get Repositories by language request
    static func getRepositories(by language: String, page: Int, completion:@escaping (Result<Repositories>)->Void) {
        
        Alamofire.request(APIRouter.getRepositories(by: language, page: page))
            .responseJSON() { response in
                guard let data = response.data else { return }
                guard response.result.isSuccess else { return }
                
                var repositories: Repositories?
                
                do {
                    repositories = try JSONDecoder().decode(Repositories.self, from: data)
                }
                catch {
                    let result = Result<Repositories>.failure(error)
                    completion(result)
                    return
                }
                
                let result = Result<Repositories>.success(repositories!)
                completion(result)
        }
    }
    
    //MARK: - Get PullRequests by creator and repository request
    static func getPullRequests(by creator: String, repository: String, completion:@escaping (Result<[PullRequest]>)->Void) {
        
        Alamofire.request(APIRouter.getPullRequests(by: creator, repository: repository))
            .responseJSON() { response in
                guard let data = response.data else { return }
                guard response.result.isSuccess else { return }
                
                var pullRequests: [PullRequest] = []
                
                do {
                    pullRequests = try JSONDecoder().decode([PullRequest].self, from: data)
                }
                catch {
                    let result = Result<[PullRequest]>.failure(error)
                    completion(result)
                    return
                }
                
                let result = Result<[PullRequest]>.success(pullRequests)
                completion(result)
        }
    }
}
