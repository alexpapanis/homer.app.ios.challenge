//
//  FirebaseAnalytics.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 01/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import FirebaseAnalytics

class FirebaseAnalyticsHelper {
    
    static func viewRepositoryDetailsEventLogger(repositoryName: String){
        
        Analytics.logEvent("viewDetails", parameters: [
            "repositoryName": repositoryName as NSObject
            ])
    }
    
    static func isNotConnectedEventLogger(){
        Analytics.logEvent("isNotConnected", parameters: nil)
    }
}
