//
//  RepositoryViewModel.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import RxSwift
import RxCocoa  

class RepositoryViewModel {
    
    //MARK: - Variables
    private let repository: Repository
    
    //MARK: - Init
    init(_ repository: Repository) {
        self.repository = repository
    }
    
    //MARK: - Repository properties
    var name: String {
        return repository.name
    }
    
    var description: String {
        return repository.description
    }

    var forks: Int {
        return repository.forks
    }

    var stars: Int {
        return repository.stars
    }
    
    var avatarUrl: String? {
        guard let user = repository.user else {return nil}
        return user.avatarUrl
    }
    
    var login: String? {
        guard let user = repository.user else {return nil}
        return user.login
    }
    
}

