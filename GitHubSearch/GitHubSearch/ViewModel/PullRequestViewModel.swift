//
//  PullRequestViewModel.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import Foundation

class PullRequestViewModel {
    
    //MARK: - Variables
    private let pullRequest: PullRequest
    
    //MARK: - Init
    init(_ pullRequest: PullRequest) {
        self.pullRequest = pullRequest
    }
    
    //MARK: - PullRequest properties
    
    var title: String {
        return pullRequest.title
    }
    
    var body: String {
        return pullRequest.body ?? ""
    }
    
    var avatarUrl: String? {
        guard let user = pullRequest.user else {return nil}
        return user.avatarUrl
    }
    
    var login: String? {
        guard let user = pullRequest.user else {return nil}
        return user.login
    }
    
    var url: String {
        return pullRequest.title
    }
    
    var date: String {
        return pullRequest.date.convertDateStringToString()
        
    }
    
}

