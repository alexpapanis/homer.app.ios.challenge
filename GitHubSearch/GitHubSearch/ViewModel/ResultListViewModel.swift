//
//  ResultListViewModel.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 01/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import RxSwift
import RxCocoa

import Foundation

class ResultListViewModel {
    //MARK: - Variables
    private var repositoryList = BehaviorRelay<[Repository]>(value: [])
    private var page = 0
    
    //MARK: - RXObservable
    var repositoriesObservable: Observable<[Repository]> {
        return self.repositoryList.asObservable()
    }
    
    //MARK: - Subscript
    //Load more repositories when last less than 5 items
    subscript(index: Int) -> RepositoryViewModel {
        if index > count - 5 {
            fetchMoreRepositories(language: Defines.LANGUAGE)
        }
        return RepositoryViewModel(repositoryList.value[index])
    }
    
    var count: Int {
        return repositoryList.value.count
    }
    
    //MARK: - Init
    init(language: String) {
        fetchMoreRepositories(language: Defines.LANGUAGE)
    }
    
    //MARK: - Functions
    func removeAll() {
        repositoryList = BehaviorRelay<[Repository]>(value: [])
    }
    
    //Fetch More Repositories
    private func fetchMoreRepositories(language: String) {
        page = page + 1
        APIClient.getRepositories(by: language, page: page) { [weak self] result in
            if let strongSelf = self {
                switch result {
                case .success(let repositories):
                    strongSelf.repositoryList.accept(strongSelf.repositoryList.value + repositories.repository)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
        }
    }
}
