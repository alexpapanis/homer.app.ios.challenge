//
//  RepositoriesViewController.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import Lottie

class RepositoriesViewController: UIViewController, NoInternetConnectionDelegate {
    
    //MARK: - Variables
    fileprivate var resultListViewModel: ResultListViewModel?
    fileprivate let disposeBag = DisposeBag()
    
    fileprivate let repositoryCell = "RepositoryCell"
    fileprivate var repositories: [Repository] = []
    
    fileprivate var animation: AnimationView!
    
    //MARK: - IB Outlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var animationView: UIView!
    
    //MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.resultListViewModel = ResultListViewModel(language: Defines.LANGUAGE)
        self.loading()
    }
    
    //MARK: - Functions
    fileprivate func loading() {
        
        //check if there is internet connection
        if !Reachability.isConnectedToNetwork(){
            //if not, show message and create Firebase's event
            print("Internet Connection is not available!")
            self.performSegue(withIdentifier: "showNoInternetConnection", sender: nil)
            FirebaseAnalyticsHelper.isNotConnectedEventLogger()
        }else{
            //if there is connectivity, show loading animation and retrieve the repositories
            print("Internet Connection is available!")

            self.loadingView.isHidden = false
            self.startLoading()
            self.setupGetRepositoriesViewModelObserver()
        }
        
    }
    
    fileprivate func startLoading() {
        self.loadingView.isHidden = false
        
        self.animation = AnimationView(name: "hot-coffee")
        self.animation.frame = CGRect(x: 0, y: 0, width: animationView.frame.size.width, height: animationView.frame.size.height)
        self.animation.contentMode = .scaleAspectFit
        self.animation.loopMode = .loop
        animationView.addSubview(self.animation)
        self.startAnimation()
        
        //When app enters in background, the animation is suspended. With this notification, the animation should continue as soon as the app become active again.
        NotificationCenter.default.addObserver(self, selector: #selector(self.startAnimation), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc fileprivate func startAnimation(){
        animation.play()
    }
    
    fileprivate func stopAnimation() {
        self.animation.stop()
    }

    
    //MARK: - Rx Setup
    fileprivate func setupGetRepositoriesViewModelObserver() {
        if Reachability.isConnectedToNetwork(){
            //initialize Observable of resultListViewModel
            self.resultListViewModel?.repositoriesObservable
                .subscribe(onNext: { repositories in
                    
                    self.repositories = repositories
                    self.tableView.reloadData()
                    
                    //if the results aren't ready yet, show the loading view
                    if repositories.count > 0 {
                        self.loadingView.isHidden = true
                    } else {
                        self.loadingView.isHidden = false
                    }
                    
                    //if the results are empty, show the no results view
                    if self.resultListViewModel?.count == 0 {
                        self.noResultsView.isHidden = false
                    } else {
                        self.noResultsView.isHidden = true
                    }
                })
                .disposed(by: disposeBag)
        } else {
            self.performSegue(withIdentifier: "showNoInternetConnection", sender: nil)
            FirebaseAnalyticsHelper.isNotConnectedEventLogger()
        }
    }

    //MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let pullRequestVC = segue.destination as? PullRequestsViewController {
            if let repository = sender as? Repository {
                pullRequestVC.repository = repository
            }
        }
        
        if let noInternetConnectionVC = segue.destination as? NoInternetConnectionViewController {
            noInternetConnectionVC.delegate = self
        }
    }
    
    //MARK: - No Internet Connection Delegate functions
    func reloadPage() {
        loading()
    }
}

//MARK: - UITableView extension
extension RepositoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: repositoryCell) as! RepositoryCell
        
        //initialize viewModel regarding its position in page of results
        _ = resultListViewModel?[indexPath.row]
        cell.repositoryViewModel = RepositoryViewModel(repositories[indexPath.row])
        
        return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        performSegue(withIdentifier: "showPullRequests", sender: repositories[indexPath.row])
    }
    
}
