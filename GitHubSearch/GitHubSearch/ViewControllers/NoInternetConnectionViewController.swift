//
//  NoInternetConnectionViewController.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 02/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import UIKit

protocol NoInternetConnectionDelegate {
    func reloadPage()
}

class NoInternetConnectionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    var delegate: NoInternetConnectionDelegate!

    @IBAction func retry(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.delegate.reloadPage()
        })
    }
    
}
