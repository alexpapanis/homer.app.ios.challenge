//
//  PullRequestsViewController.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class PullRequestsViewController: UIViewController, NoInternetConnectionDelegate {

    //MARK: - Variables
    fileprivate let repositorioCell = "PullRequestCell"
    fileprivate var animation: AnimationView!
    fileprivate var pullRequests: [PullRequest] = []
    
    var repository: Repository?
    
    //MARK: - IB Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var vwNoResults: UIView!
    @IBOutlet weak var animationView: UIView!
    
    //MARK: - ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loading()    
    }
    
    //MARK: - Functions
    fileprivate func loading() {
        
        //check if there is internet connection
        if !Reachability.isConnectedToNetwork(){
            //if not, show message and create Firebase's event
            print("Internet Connection is not available!")
            performSegue(withIdentifier: "showNoInternetConnection", sender: nil)
            FirebaseAnalyticsHelper.isNotConnectedEventLogger()
        }else{
            //if there is connectivity, show loading animation and retrieve the repositories
            print("Internet Connection is available!")
            
            self.loadingView.isHidden = false
            self.startLoading()
            self.getPullRequests()
        }
    }
    
    fileprivate func startLoading() {
        self.loadingView.isHidden = false
        
        self.animation = AnimationView(name: "hot-coffee")
        self.animation.frame = CGRect(x: 0, y: 0, width: animationView.frame.size.width, height: animationView.frame.size.height)
        self.animation.contentMode = .scaleAspectFit
        self.animation.loopMode = .loop
        animationView.addSubview(self.animation)
        self.startAnimation()
        
        //When app enters in background, the animation is suspended. With this notification, the animation should continue as soon as the app become active again.
        NotificationCenter.default.addObserver(self, selector: #selector(self.startAnimation), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc fileprivate func startAnimation(){
        animation.play()
    }
    
    fileprivate func stopAnimation() {
        self.animation.stop()
    }
    
    fileprivate func getPullRequests(){
        if let repository = self.repository {
            APIClient.getPullRequests(by: repository.user!.login, repository: repository.name, completion: { [weak self] result in
                if let strongSelf = self {
                    switch result {
                    case .success(let pullRequests):
                        strongSelf.pullRequests = pullRequests
                        strongSelf.loadingView.isHidden = true
                        
                        if pullRequests.count > 0 {
                            strongSelf.vwNoResults.isHidden = true
                            strongSelf.tableView.isHidden = false
                            strongSelf.tableView.reloadData()
                        } else {
                            strongSelf.tableView.isHidden = true
                            strongSelf.vwNoResults.isHidden = false
                        }
                        
                    case .failure(let error):
                        print(error.localizedDescription)
                        strongSelf.loadingView.isHidden = true
                        strongSelf.vwNoResults.isHidden = false
                    }
                }
            })
        } else {
            //erro
            print("empty pullRequests")
        }
    }
    
    @objc fileprivate func continueAnimation() {
        animation!.play()
    }
    
    //MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let noInternetConnectionVC = segue.destination as? NoInternetConnectionViewController {
            noInternetConnectionVC.delegate = self
        }
    }
    
    //MARK: - No Internet Connection Delegate functions
    func reloadPage() {
        loading()
    }
}

//MARK: - UITableView extension
extension PullRequestsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: repositorioCell) as! PullRequestCell
        cell.pullRequestViewModel = PullRequestViewModel(self.pullRequests[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        
        if let url = URL(string: pullRequests[indexPath.row].url) {
            UIApplication.shared.open(url)
        }
    }
}
