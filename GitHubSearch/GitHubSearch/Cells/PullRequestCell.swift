//
//  PullRequestCell.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 31/10/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import UIKit
import SDWebImage

class PullRequestCell: UITableViewCell {

    //MARK: - IB Outlets
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbDescription: UILabel!
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbLogin: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    
    //MARK: - PullRequestCell properties
    var pullRequestViewModel: PullRequestViewModel! {
        didSet{
            self.updateUI()
        }
    }
    
    //MARK: - UPDATE UI
    func updateUI(){
        lbTitle.text = pullRequestViewModel.title
        lbDescription.text = pullRequestViewModel.body
        lbLogin.text = pullRequestViewModel.login
        lbDate.text = pullRequestViewModel.date
        
        if let avatarUrl = pullRequestViewModel.avatarUrl {
            imgAvatar.sd_setImage(with: URL(string: avatarUrl))
        } else {
            imgAvatar.image = UIImage(named: "icon-user-placeholder")
        }
                
        DispatchQueue.main.async {
            self.imgAvatar.cornerRadius = self.imgAvatar.frame.width/2
        }
    }

}
