//
//  Defines.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 01/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import Foundation

class Defines {
    static let URL_REPOSITORIES = "https://api.github.com/search/repositories"
    static let URL_PULLREQUESTS = "https://api.github.com/repos"
    static let LANGUAGE = "SWIFT"
}
