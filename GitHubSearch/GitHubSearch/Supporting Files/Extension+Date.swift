//
//  Extension+Date.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 02/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import Foundation

extension String {
    func convertDateStringToString() -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = df.date(from: self)!
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let finalDate = calendar.date(from:components)
        df.dateFormat = "dd-MM-yyyy"
        return df.string(from: finalDate!)
    }
}

