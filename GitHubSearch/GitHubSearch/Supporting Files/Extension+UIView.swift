//
//  Extension+UIView.swift
//  GitHubSearch
//
//  Created by Alexandre Papanis on 01/11/19.
//  Copyright © 2019 Papanis. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}
